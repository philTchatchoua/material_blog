<?php 

	/*
	*  ../Noyau/fonctions.php
	*  Fonction personnalisées du framework
	*/
namespace Noyau\fonctions;

function formater_date(string $date, string $format = 'D d M Y') :string {
  return date_format(date_create($date), $format);
}