<?php 
/*
*   ../App/Modeles/postsModele.php
*   Modeles - posts
*/


	namespace Modele\posts;


	function findAll(\PDO $connexion, $params = []){
		
		$params_default =[
			'orderBy'=>'id',
			'orderSens'=>'ASC',
			'limit' => null
				
		 ];
		
		$params= array_merge($params_default, $params);
		
		
		$orderBy = htmlentities($params['orderBy']);
		$orderSens = htmlentities($params['orderSens']);
		
		$sql ="SELECT *, posts.id AS postsId, posts.slug AS postSlug,posts.titre as titrePost
				FROM posts 
				INNER JOIN auteurs
				USING(id)
				ORDER BY $orderBy $orderSens ";
		$sql .=($params['limit'] !== null)?"LIMIT:limit ;":';';
		 
		$rs = $connexion->prepare($sql);
		if($params['limit'] !== null):
			$rs->bindvalue(':limit', $params['limit'], \PDO::PARAM_INT);
		endif;
		
		$rs ->execute();
		
		return $rs->fetchAll(\PDO::FETCH_ASSOC);
		
	}


	function findOneById(\PDO $connexion, int $id){
			$sql="
				SELECT*
				FROM posts
				INNER JOIN auteurs
				USING(id)
				WHERE posts.id =:id;";
		
		$rs = $connexion->prepare($sql);
		$rs->bindValue(':id',$id, \PDO::PARAM_INT);
		$rs->execute();
		
		return $rs->fetch(\PDO::FETCH_ASSOC);
	}

	function findPostsByCategory(\PDO $connexion, int $id){
			
			$sql ="
			select *,pts.titre as titrePost, pts.id as postsId, pts.slug as postSlug
			from categories as cat
			inner join posts_has_categories as PHC
			on cat.id = PHC.categorie
			inner join posts as pts
			on pts.id = PHC.post
			inner join auteurs as ats
			on pts.auteur = ats.id
			where cat.id = :id;";
		
			$rs = $connexion->prepare($sql);
			$rs ->bindValue(':id',$id,\PDO::PARAM_INT);
			$rs->execute();
			return $rs->fetchAll(\PDO::FETCH_ASSOC);
		}


	function findPostBySearch(\PDO $connexion, string $search){
		
		$words = explode(' ',trim($search));
		
		$sql ="
		SELECT DISTINCT 
			pts.titre as titrePost,
			pts.id as postsId,
			pts.slug AS postSlug,
			texte,media,datePublication,pseudo
		FROM auteurs as ats
		INNER JOIN posts as pts
		ON ats.id = pts.auteur
		INNER JOIN posts_has_categories as pht
		ON pts.id = pht.post
		INNER JOIN categories as cat
		ON pht.categorie  = cat.id
		where 1 = 0";
			for($i=0; $i< count($words); $i++):
			$sql .= "
				  OR pseudo   LIKE  :word$i
				  OR pts.titre LIKE :word$i
				  OR texte 	   LIKE :word$i
				  OR cat.titre LIKE :word$i";
				  
			endfor;
			$sql.=";";
		

			$rs = $connexion->prepare($sql);
			
			for($i=0; $i< count($words); $i++):
				$rs->bindValue(":word$i",'%'.$words[$i].'%', \PDO::PARAM_STR);
			endfor;
			$rs->execute();

			
			return $rs->fetchAll(\PDO::FETCH_ASSOC);
		}






















