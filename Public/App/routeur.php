<?php 
/*
 * ../App/routeur.php
 * -- Routeur 
*/


	if(isset($_GET['users'])):
		//Routeurs des users
		include'../App/Routeurs/usersRouteur.php';

	elseif(isset($_GET['categories'])):
		//Routeur des categories
		include'../App/Routeurs/categoriesRouteur.php';

	elseif(isset($_GET['posts'])):
		//Routeur des posts
		include'../App/Routeurs/postsRouteur.php';

	// ROUTE PAR DEFAUT
	// PATTERN: /
	// CTRL: postsControleur
	// ACTION : index
	else:	
		include_once'../App/Controleurs/postsControleur.php';
		Controleur\posts\indexAction($connexion, [
			'orderby' =>"datePublication",
			'orderSens' =>"DESC"
		]);

	endif;
		

