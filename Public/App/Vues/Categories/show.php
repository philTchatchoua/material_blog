<?php 
	/*
	*  ../App/xxxx/xxx.php
	*
	*/

?>

 <h1 class="page-header">
     <?php echo $categorie['titre']; ?>
     <small>made with love</small>
 </h1>

 <?php foreach($postsCategories  as $postsCategorie): ?>
 
     
 <h2>
	 <a href="posts/<?php echo $postsCategorie['postsId']; ?>/<?php echo $postsCategorie['postSlug']; ?>"><?php echo $postsCategorie['titrePost'];?></a>
 </h2>
 <p class="lead">
   by <a href="#"><?php echo $postsCategorie['pseudo']; ?></a>
 </p>
 <p>Posted on 
 	<?php 
	 echo \Noyau\fonctions\formater_date($postsCategorie['datePublication']);
	  ?> 
 </p>
 <hr>
 <img class="img-responsive z-depth-2" src="<?php echo $postsCategorie['media']; ?>" alt="">
 <hr>
	<div><?php echo $postsCategorie['texte']; ?></div>
 <a href="posts/<?php echo $postsCategorie['postsId']; ?>/<?php echo $postsCategorie['postSlug']; ?>">
   <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
 </a>
 <hr>
<?php endforeach; ?>
