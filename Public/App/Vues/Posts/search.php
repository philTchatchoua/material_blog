<?php 
	/*
	*  
	*	../App/Vues/Posts/seach.php
	*   Variables disponibles
	*      -$posts ARRAY(ARRAY(id, titre, slug, datePublication, texte,media,auteur))
	*/


?>



 <h1 class="page-header">
     Resultat dela recherche
     <small><?php echo $search; ?></small>
 </h1>
     
 <?php foreach($posts as $post): ?>
 <h2>
	 <a href="posts/<?php echo $post['postsId']; ?>/<?php echo $post['postSlug']; ?>"><?php echo $post['titrePost'];?></a>
 </h2>
 <p class="lead">
   by <a href="#"><?php echo $post['pseudo']; ?></a>
 </p>
 <p>Posted on <?php 
	 $date = date_create($post['datePublication']);
	 echo date_format($date, 'D M Y');
	 
	  ?> </p>
 <hr>
 <img class="img-responsive z-depth-2" src="<?php echo $post['media']; ?>" alt="">
 <hr>
	<div><?php echo $post['texte']; ?></div>
 <a href="posts/<?php echo $post['postsId']; ?>/<?php echo $post['postSlug']; ?>">
   <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
 </a>
 <hr>
<?php endforeach; ?>