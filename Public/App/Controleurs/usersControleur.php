<?php 
	/*
	*  ../App/Controleur/usersControleur.php
	*
	*/

	namespace Controleur\Users;
	use Modele\Users;

	function loginFormAction(\PDO $connexion){
	
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = TITRE_CONNEXION_LOGINFORM;
		ob_start();
			include'../App/Vues/Users/formLoginUsers.php';
		$content = ob_get_clean();
	}
	

	function loginAction(\PDO $connexion, array $data = null){
		//je demande au modele le user qui correspond au login/pwd
		include_once'../App/Modeles/usersModeles.php';
		$user = Users\findOneByLoginPwd($connexion, $data);
		
		//je redirige vers l eback office si ok
		//Et vers le formulaire de connexion
		if($user):
			$_SESSION['user'] = $user;
			header('location: '. ROOT_ADMIN);
		
		else:
			header('location: ' . ROOT . 'users/login/form');

		endif;
		
	}
