<?php
	/*
	*  ../App/Categories/categoriesControleur.php
	*
	*/


	namespace Controleur\Categories;
	use Modele\Categories as Categorie;
	use Modele\Posts as Post;


	/*
	*  Name function : categorieAction
	*  Details: affiche la liste des categories
	*/

	function categorieAction(\PDO $connexion){
	
		//je demande la liste de caegorie au modèle
		include_once'../App/Modeles/categoriesModele.php';
		$categories = Categorie\findAll($connexion);
		
		//je charge la vue dans la variable $content
		include'../App/Vues/Categories/index.php';
	
	}
	
	
	/*
	*  Name function :showAction
	*  Details: affiche la liste de posts de lz categorie
	*/

	function showAction(\PDO $connexion, int $id){
	
		//je demande la liste des posts de la categorie  au modèle
		include_once'../App/Modeles/postsModele.php';
		$postsCategories = Post\findPostsByCategory($connexion, $id);
		
		include_once'../App/Modeles/categoriesModele.php';
		$categorie = Categorie\findCategorieById($connexion, $id);
		
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = $categorie['titre'];
		
		ob_start();
			include'../App/Vues/Categories/show.php';
		$content = ob_get_clean();
	}
