<?php 
/*
*   ../App/Controleurs/postsControleur.php
*   Controleur - Posts
*/

	namespace Controleur\posts;
	use Modele\posts;


	
	function indexAction(\PDO $connexion, array $params = []){
		
		//je demande la liste des posts au modèle
		include_once'../App/Modeles/postsModele.php';
		$posts = posts\findAll($connexion, $params);
		
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = TITRE_POSTS_INDEX;
		
		ob_start();
			include'../App/Vues/Posts/index.php';
		$content = ob_get_clean();
	}


	function showAction(\PDO $connexion, int $id){
		//je demande de details de posts au modèle
		include_once'../App/Modeles/postsModele.php';
		$post =posts\findOneById($connexion, $id);
		
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = $post['titre'];
		
		ob_start();
			include'../App/Vues/Posts/show.php';
		$content = ob_get_clean();
	}



	function searchAction(\PDO $connexion, string $search){
	
		//je demande les posts contenant le search au modèle
		include_once'../App/Modeles/postsModele.php';
		$posts = posts\findPostBySearch($connexion, $search);
		
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = $search;
		
		ob_start();
			include'../App/Vues/Posts/search.php';
		$content = ob_get_clean();
	}

	





