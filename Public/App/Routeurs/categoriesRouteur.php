<?php 

	/*
	*  ../App/Routeur/categorieRouteur.php
	*
	*/

	use Controleur\Categories;
	include_once'../App/Controleurs/categoriesControleur.php';

	switch ($_GET['categories']):

		case 'show':
			// AFFICHAGE DES POSTS D'UN CATEGORIE
			// PATTERN:/index.php?categories=show&id=X
			// CTRL: categoriesControleur
			// ACTION : show
			Categories\showAction($connexion, $_GET['id']);	
			break;

	endswitch;