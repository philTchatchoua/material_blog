<?php 

	/*
	*  ../App/Routeurs/usersRouteur.php
	* 
	*/
	
	use Controleur\Users;

	switch ($_GET['users']):
		case 'loginForm':
			// ROUTE D'AFFICHAGE DU FORMULAIRE
			// PATTERN: /index.php?users=loginForm
			// CTRL: usersControleur
			// ACTION : loginForm
			include_once'../App/Controleurs/usersControleur.php';
			Users\loginFormAction($connexion);	
			break;

		case 'login':
			// ROUTE D'ENVOIE DU FORMULAIRE
			// PATTERN: /index.php?users=login
			// CTRL: usersControleur
			// ACTION : login
			 $data = [
				'login' => $_POST['pseudo'],
				 'pwd'  => $_POST['mdp'] 
			 ];
			include_once'../App/Controleurs/usersControleur.php';
			Users\loginAction($connexion, $data);	
			break;
	endswitch;