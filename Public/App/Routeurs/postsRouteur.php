<?php 

	/*
	*  ../App/Routeur/postsRouteur.php
	*
	*/

	use Controleur\posts;
	include_once'../App/Controleurs/postsControleur.php';

 	switch ($_GET['posts']):
 
 		case 'show':
		// DETAIL D'UN POST
		// PATTERN: /index.php?posts=show&id=X
		// CTRL: postsControleur
		// ACTION : show
		posts\showAction($connexion, $_GET['id']);
 			 break;

 		case 'search':
		// RECHERCHE D'UN POST
		// PATTERN: /index.php?posts=search
		// CTRL: postsControleur
		// ACTION : search
		posts\searchAction($connexion, $_POST['search']);
 			 break;
 		
 	endswitch;