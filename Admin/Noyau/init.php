<?php
	/*
	* ../Noyau/constantes.php
	*
	*/
		if(!isset($_SESSION)):
			session_start();
		endif;
	
	    require_once'../App/Config/parametres.php';
		require_once'../Noyau/connexion.php';
		require_once'../Noyau/constantes.php';

		if(!isset($_SESSION['user'])):
			header('location: '. ROOT_PUBLIC .'users/login/form');
		endif;
		require_once'../Noyau/fonctions.php';
