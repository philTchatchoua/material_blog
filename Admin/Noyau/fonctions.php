<?php 

	/*
	*  ../Noyau/fonctions.php
	*  Fonction personnalisées du framework
	*/
namespace Noyau\fonctions;





/*
---------------------------------------------
TRAITEMENT DES DATES
---------------------------------------------
 */

/**
 * Slugifier une chaine de caractère
 * @param  string $date
 * @param  string $format[delimiter par defaut]
 * @return string
 */

function formater_date(string $date, string $format = 'D d M Y') :string {
  return date_format(date_create($date), $format);
}




/*
---------------------------------------------
TRAITEMENT DES SLUG
---------------------------------------------
 */

/**
 * Slugifier une chaine de caractère
 * @param  string $texte
 * @param  string $delimiter [delimiter par defaut]
 * @return string
 */
 function slugify($string, $delimiter = '-') {
    $oldLocale = setlocale(LC_ALL, '0');
    setlocale(LC_ALL, 'en_US.UTF-8');
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower($clean);
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    $clean = trim($clean, $delimiter);
    setlocale(LC_ALL, $oldLocale);
    return $clean;
  }