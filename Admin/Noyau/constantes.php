<?php

/*
*  ../Noyau/constantes.php
*  Constantes personalisées pour framework
*/

 $local_path = str_replace(basename($_SERVER['SCRIPT_NAME']) , '', $_SERVER['SCRIPT_NAME']);
  
  //Redicrection vers la page d'acceuil 	
  define('ROOT', 'http://'
                . $_SERVER['HTTP_HOST']
                . $local_path);
	
  
  //Redirection vers la page ADMIN du backoffice	
  define('ROOT_PUBLIC', 'http://'
                . $_SERVER['HTTP_HOST']
                . str_replace(ADMIN_FOLDER, PUBLIC_FOLDER,str_replace(basename($_SERVER['SCRIPT_NAME']) , '', $_SERVER['SCRIPT_NAME'])));

