<?php
	/*
	*  ../App/Vues/Categories/index.php
	*  Variables disponibles:
				Array( array(id, titre, slug))
	*/
?>


<h1><?php echo TITRE_LISTE_CATEGORIES; ?></h1>
<div><a href="categories/add/form">Ajouter un enregistrement</a></div>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>Id</th>
      <th>Titre</th>
      <th>Slug</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
	<?php  foreach($categories as $categorie):?>
	  <tr>
	   <td><?php echo $categorie['id']; ?></td>
	   <td><?php echo $categorie['titre']; ?></td>
	   <td><?php echo $categorie['slug']; ?></td>
	   <td>
		 <a href="categories/edit/<?php echo $categorie['id'];?>">Edit</a> |
			<a href="categories/<?php echo $categorie['id'];?>/delete" class="delete">Delete</a>
	   </td>
    </tr>
    <?php  endforeach;?>
     
     
    </tbody>
</table>