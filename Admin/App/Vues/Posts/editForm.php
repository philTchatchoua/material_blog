<?php 
	/*
	*  ../App/Vues/Posts/addForm.php
	*  $post Array(id,titre, slug, texte, auteur)
	*  $auteurs Array(Array(id, pseudo,mdp))
	*/
?>

<h1>Edition d'un post</h1>
<div>
  <a href="<?php echo ROOT; ?>posts">Retour vers la liste des posts</a>
</div>

<form action="posts/<?php echo $post['id']; ?>/update" method="post">
	<fieldset>
	<legend>Données du post</legend>
	  <div>
		<label for="titre">Titre</label>
		<input type="text" name="titre" id="titre" value="<?php echo $post['titre']; ?>" />
	  </div>
	  <div>
		<label for="slug">Slug</label>
		<input type="text" name="slug" id="slug" value="<?php echo $post['slug']; ?>" />
	  </div>
	  <div>
		<label for="texte">Texte</label>
		<textarea name="texte" id="texte"><?php echo $post['texte']; ?></textarea>
	  </div>
	  <div>
		<label for="media">Media</label>
		<input type="file" name="media" id="media" />
	  </div>

	  <!-- MENU DEROULANT DYNAMIQUE -->
	  <div>
		<label for="<?php echo $auteur['pseudo']; ?>">Auteur</label>
			<select name="auteur" id="auteur">
			<?php foreach($auteurs as $auteur):?>
				<option value="<?php echo $auteur['id'];?>" <?php if($auteur['id']== $post['auteur']){echo 'selected="selected"';}?>><?php echo $auteur['pseudo']; ?></option>
			<?php endforeach;?>
			</select>
	  </div>
	  <!-- FIN MENU DEROULANT DYNAMIQUE -->
	</fieldset>

<fieldset>
  <legend>Catégories</legend>
  <!-- LISTE DYNAMIQUE DE CHECKBOXES -->
  <div>
  <?php foreach($categories as $categorie):;?>
	  <input type="checkbox" name="categories[]"  value="<?php echo $categorie['id']; ?>"id="<?php echo $categorie['slug']; ?>" <?php if($categorie['id']== $postsCategorie['categorie']){echo'checked="checked"';}?>/>
	  <label for="<?php echo $categorie['slug']; ?>"><?php echo $categorie['titre']; ?></label><br/>
   <?php endforeach; ?>
  </div>
  <!-- FIN LISTE DYNAMIQUE DE CHECKBOXES -->
</fieldset>
  <div><input type="submit" /></div>
</form>