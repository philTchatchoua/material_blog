<?php 
	/*
	 * ../App/Vues/Templates/default.php
	 * -- templete par default 
	 */
 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<?php include'../App/Vues/Templates/Partials/head.php';?>
  </head>
  <body>
	<!-- Fixed navbar -->
		<?php include'../App/Vues/Templates/Partials/nav.php';?>

    <div class="container theme-showcase" role="main">
      <!-- Main jumbotron for a primary marketing message or call to action -->
		<div class="jumbotron">
			<?php echo $content; ?>
    	 </div>
    </div>
    <!-- /container -->
    <!-- Scripts -->
        <!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
		<?php include'../App/Vues/Templates/Partials/scripts.php';?>
	</body>
</html>



