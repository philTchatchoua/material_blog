<?php 
	/*
	*   ../App/Modeles/postsHasCategorieModele.php
	*   Controleur - Posts_has_categories
	*/
	namespace Modele\postsHasCategories;

	function findPostsHasCategorieById(\PDO $connexion, int $id){
			$sql ="
				 SELECT*
				 FROM posts_has_categories
				 WHERE post = :id;
				";
		$rs = $connexion ->prepare($sql);
		$rs ->bindvalue(':id',$id, \PDO::PARAM_INT);
		$rs->execute();
		return $rs->fetch(\PDO::FETCH_ASSOC);
	}


