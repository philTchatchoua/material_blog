<?php 
/*
*   ../App/Modeles/postsModele.php
*   Modeles - posts
*/


	namespace Modele\posts;


	function findAll(\PDO $connexion, $params = []){
		
		$params_default =[
			'orderBy'	=>'id',
			'orderSens' =>'asc',
			'limit'		=> null		
		 ];	
		$params= array_merge($params_default, $params);
		$orderBy = htmlentities($params['orderBy']);
		$orderSens = htmlentities($params['orderSens']);
		
		$sql ="
				SELECT*,posts.id as postId
				FROM posts
				INNER JOIN auteurs 
				ON auteurs.id = posts.auteur
				ORDER BY posts.id $orderSens";
		
		if($params['limit']!== null):
			$sql.="Limit :limit";
			$rs=$connexion->prepare($sql);
		    $rs->bindvalue(':limit', $params['limit'], \PDO::PARAM_INT);
			$rs ->execute();
		
		else: 
		    $rs=$connexion->query($sql);
		
		endif;	 
		return $rs->fetchAll(\PDO::FETCH_ASSOC);
		
	}



	function insertOne(\PDO $connexion, array $data){
		$sql="
			INSERT INTO posts(id,titre, slug, datePublication, texte,auteur)
			VALUES(null,:titre, :slug,NOW(), :texte, :auteur);
		";
		
		$rs = $connexion->prepare($sql);
		$rs ->bindValue(':titre',$data['titre'], \PDO::PARAM_STR);
		$rs ->bindValue(':slug',$data['slug'], \PDO::PARAM_STR);
		$rs ->bindValue(':texte',$data['texte'], \PDO::PARAM_STR);
		$rs ->bindValue(':auteur',$data['auteur'], \PDO::PARAM_INT);
		$rs->execute();
		
		return $connexion ->lastInsertId();
	}


	function insertCategorieById(\PDO $connexion, array $data){
		
		 $sql = "INSERT INTO posts_has_categories
				SET post = :post,
					categorie = :categorie;";
		
		$rs = $connexion ->prepare($sql);
		$rs ->bindValue(':post',$data['postID'], \PDO::PARAM_INT);
		$rs ->bindValue(':categorie',$data['categorieID'], \PDO::PARAM_INT);
		return $rs->execute();
		
		
		
	}


	function deletePosts_has_categorieByPostID(\PDO $connexion, int $postID) :bool{
		$sql = "
				DELETE
				FROM posts_has_categories
				WHERE post = :postID;";
		
		$rs = $connexion ->prepare($sql);
		$rs ->bindValue(':postID',$postID, \PDO::PARAM_INT);
		return $rs->execute();
	
	}


	function deletePostsById(\PDO $connexion, int $id){
		$sql = "
				DELETE
				FROM posts
				WHERE id =:id;
				";
		$rs = $connexion ->prepare($sql);
		$rs ->bindValue(':id',$id, \PDO::PARAM_INT);
		return $rs->execute();	
	}


	function editFormByPostId(\PDO $connexion, int $id){
			$sql = "
					SELECT*
					FROM posts
					WHERE id = :id;
					";
			$rs = $connexion ->prepare($sql);
			$rs ->bindValue(':id',$id, \PDO::PARAM_INT);
			$rs->execute();	
			return $rs->fetch(\PDO::FETCH_ASSOC);
	 }


/*-----------------------------------------------------------------------
						PARTIE UPDATE DED POSTS
-------------------------------------------------------------------------*/

		function  updatePostsById(\PDO $connexion, array $data ){
			
			$sql ="
				UPDATE posts
					SET titre =:titre,
					slug   = :slug,
					texte  = :texte,
					auteur = :auteur,
					datePublication = now()
					WHERE id = :id;
					";
			$rs = $connexion->prepare($sql);
			$rs->bindvalue(':titre',$data['titre'],\PDO::PARAM_STR);
			$rs->bindvalue(':slug',$data['slug'],\PDO::PARAM_STR);
			$rs->bindvalue(':texte',$data['texte'],\PDO::PARAM_STR);
			$rs->bindvalue(':auteur',$data['auteur'],\PDO::PARAM_INT);
			$rs->bindvalue(':id',$_GET['id'],\PDO::PARAM_INT);
			return $rs->execute();	
			
			
	
		}

			function updateCategorieByPostId(\PDO $connexion, array $data){
			$sql ="
				UPDATE posts_has_categories
					SET categorie = :categorie	
					WHERE post = :post;
					";
			$rs = $connexion->prepare($sql);
			$rs->bindvalue(':post',$data['postId'],\PDO::PARAM_INT);
			$rs->bindvalue(':categorie',$data['categorie'],\PDO::PARAM_INT);
	
			return $rs->execute();
		}












