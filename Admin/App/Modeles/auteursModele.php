<?php
	/*
	*  ../App/Modeles/auteurModele.php
	*
	*/
	 namespace Modele\Auteurs;
	


   /*
	*  function : findAll
	*  Details : revoie la liste des auteurs
	*/

	function findAll(\PDO $connexion){
		$sql="
			SELECT*
			FROM auteurs
			ORDER BY pseudo;";
		
		$rs =$connexion ->query($sql);
		return $rs->fetchAll(\PDO::FETCH_ASSOC);
	}