<?php 
	/*
	*  ../App/Modeles/categoriesModele.php
	*
	*/

	namespace Modele\Categories;

	/*
	*  Function  : findAll
	*  Details : Va cherhcer  toutes les catagories de la tables categories 
	*  Param :(connexion, id, le sens, la limit)
	*/

	
	function findAll(\PDO $connexion, array $params = []){
			//je creer un tableau par default
			$params_default=[
				'orderBy'=>'id',
				'orderSens'=>'ASC',
				'limit'=> null
				
			];
			//si il n'ya uacune valeur dans le tableau entrer en paramètre je lui attribut  les valeur du tableau par defaut et je le stocke dans une variable
			$params = array_merge($params_default, $params);
			
			//je transforme en valeur html que je met dans uen variable
			$orderBy = htmlentities($params['orderBy']);
			$orderSens= htmlentities($params['orderSens']);
		
			$sql="
				SELECT*
				FROM categories
				ORDER BY $orderBy $orderSens";
			
			$sql.= ($params['limit']!== null)?'LIMIT :limit':' ';
			
		    //test la condition sur le params['limit] pour le tableau  entrer en paramètre
			if($params['limit']!== null):
		
				$rs = $connexion ->prepare($sql);
				$rs->bindValue(':limit',$params['limit'],\PDO:: PARAM_INT);
				$rs->execute();
		
			else:
		
				$rs = $connexion ->query($sql);
		
			endif;
		
			return $rs->fetchAll(\PDO::FETCH_ASSOC);
			
		}

		/*
		 *  Function: insertOne
		 *  Details :insert une nouvelles categorie dans la DB
		 *  Param :(PDO: connexion, array $data)
		*/

		
	   function insertOne(\PDO $connexion, array $data =null){

			$sql="
				  INSERT INTO categories(id,titre,slug)
				  VALUE(null,:titre,:slug);
				  ";
		
			$rs = $connexion->prepare($sql);
			$rs->bindValue(':titre',$data['titre'], \PDO::PARAM_STR);
			$rs->bindValue(':slug',\Noyau\fonctions\slugify($data['titre']), \PDO::PARAM_STR);
			$rs->execute();
		
			return $connexion->lastInsertId();
		}
		
		/*
		 *  Function: deleteOne
		 *  Details : suppression d'une categorie
		 *  Param :(PDO: connexion, int $data)
		*/
		
	   function deleteOne(\PDO $connexion, int $data){
			$sql="
				  DELETE
				  FROM categories
				  WHERE id = :id;";
	
			$rs = $connexion->prepare($sql);
			$rs->bindValue(':id',$data, \PDO::PARAM_INT);
			return intval($rs->execute());		
		}


		/*
		 *  Function: editOne
		 *  Details : edition d'une categorie
		 *  Param :(PDO: connexion, int $data)
		*/
		  function editOne(\PDO $connexion, int $data){
			$sql="
				  SELECT*
				  FROM categories
				  WHERE id = :id;";
			  
			$rs = $connexion -> prepare($sql);
			$rs->bindValue(':id',$data, \PDO::PARAM_INT);
			$rs->execute();
			return $rs->fetch(\PDO::FETCH_ASSOC);
		 
			 
		}


		/*
		 *  Function: updateOne
		 *  Details : updated'une categorie
		 *  Param :(PDO: connexion, array  $data)
		*/
		  function updateOne(\PDO $connexion, array $data = null){
			$sql="
				  UPDATE categories
				  SET titre = :titre
				  WHERE id = :id;";
			$rs = $connexion->prepare($sql);
			$rs->bindValue(':id',$data['id'], \PDO::PARAM_INT);
			$rs->bindValue(':titre',$data['titre'], \PDO::PARAM_STR);
			return intval($rs->execute());
		}

		
