<?php 
/*
 * ../App/routeur.php
 * -- Routeur 
*/

	if(isset($_GET['posts'])):
		//ROUTE DES POSTS
		include_once'../App/Routeurs/postsRouteur.php';

	elseif(isset($_GET['categories'])):
		//ROUTE DES CATEGORIES 
		include_once'../App/Routeurs/categoriesRouteur.php';

	elseif(isset($_GET['users'])):
		//ROUTE DE DECONNEXION
		include_once'../App/Routeurs/usersRouteur.php';

	else:
		//ROUTE PAR DEFAUT
		include_once'../App/Controleurs/usersControleur.php';
		Controleur\Users\dashboardAction($connexion);

	endif;
	
	
	
		

