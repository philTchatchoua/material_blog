<?php 
/*
*   ../App/Controleurs/postsControleur.php
*   Controleur - Posts
*/

	namespace Controleur\posts;
	use Modele\posts;
	use Modele\Auteurs;
    use Modele\Categories as Categorie;
	use Modele\postsHasCategories as pHc;
	
	/*
	*  function: indexAction
	*  Details: liste des posts
	*/

	function indexAction(\PDO $connexion, array $params = []){
		
		//je demande la liste des posts au modèle
		include_once'../App/Modeles/postsModele.php';
		$posts = posts\findAll($connexion, $params);
		
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = TITRE_POSTS_INDEX;
		
		ob_start();
			include'../App/Vues/Posts/index.php';
		$content = ob_get_clean();
	}


    
	/*
	*  function : addFormAction
	*  Details : formulaire d'enregistrement des posts
	*/
	
	function addFormAction(\PDO $connexion){
		//je charge le modele de la table Auteur
		include_once'../App/Modeles/auteursModele.php';
		$auteurs = Auteurs\findAll($connexion);
		
		//je charge le modele de la table categorie
		include_once'../App/Modeles/categoriesModele.php';
		$categories = Categorie\findAll($connexion); 
			
		GLOBAL $content, $titre;
		$titre = TITRE_AJOUT_POSTS;
		ob_start();
			include'../App/Vues/Posts/addForm.php';
		$content = ob_get_clean();
	}


	/*
	*	function : insertFormAction
	*   Detials : insertion du posts
	*/

	function insertFormAction(\PDO $connexion){
		//je demande au modèle d'ajouter le post
		include_once'../App/Modeles/postsModele.php';
		$id = posts\insertOne($connexion,$_POST);
		
		//je demande au modèle d'ajouter les categories correpondante
		foreach($_POST['categories'] as $categorieID):
			$return = posts\insertCategorieById($connexion,[
				'postID'=>$id,
				'categorieID'=>$categorieID
					
			]);
		endforeach;
		//Redirection vers la liste des postes
		header('location: '.ROOT.'posts');
	}


	/*
	*	function : deleteAction
	*   Detials : suppresion d'un posts
	*/
	function deleteAction(\PDO $connexion, int $id){
		
		//je demande au modèle supprimer le posts dans la table posts_has_categories
		include_once'../App/Modeles/postsModele.php';
		$return1 = posts\deletePosts_has_categorieByPostID($connexion, $id);
		
		//je demande au modèlede supprimer le posts dans la tabel posts
		include_once'../App/Modeles/postsModele.php';
		$return2 = posts\deletePostsById($connexion, $id);
		
		//Redirection vers la liste des postes
		header('location: '.ROOT.'posts');
	}

	/*
	*	function : editAction
	*   Detials : edition d'un posts
	*/
	function editAction(\PDO $connexion, int $id){
		
		//je demande au modèle de selectionner le post à editer
		include_once'../App/Modeles/postsModele.php';
		$post= posts\editFormByPostId($connexion, $id);
		
		//je demande au modele laliste des auteurs
		include_once'../App/Modeles/auteursModele.php';
		$auteurs = Auteurs\findAll($connexion);
		
		//je demande au modele la liste des posts corespondant ausx categories
		include_once'../App/Modeles/postsHasCategoriesModele.php';
		$postsCategorie = pHc\findPostsHasCategorieById($connexion, $id);
	
		//je charge le modele de la table categorie
		include_once'../App/Modeles/categoriesModele.php';
		$categories = Categorie\findAll($connexion);
		
		//je charge la vue
		GLOBAL $content,$titre;
		$titre = TITRE_EDITION_UN_POST;
		
		ob_start();
			include'../App/Vues/Posts/editForm.php';
		$content = ob_get_clean();
		
	}


	function updateAction(\PDO $connexion){
		
		//je demande au modele de suprimier les categories corespondante au post dans la table posts_has_categorie
		include_once'../App/Modeles/postsModele.php';
		$return1 = posts\deletePosts_has_categorieByPostID($connexion, $_GET['id']);
	
		//je demande au modele de update les ancienne données
		include_once'../App/Modeles/postsModele.php';
		$returnUpadtePost = posts\updatePostsById($connexion, $_POST);
		
		//je demande au modèle d'ajouter les categories correpondante
		foreach($_POST['categories'] as $categorieID):
			$return = posts\insertCategorieById($connexion,[
				'postID'=> $_GET['id'],
				'categorieID'=>$categorieID
					
			]);
		endforeach;
		
		//je demande au modele de modifier les categories correspodante
		/*foreach($_POST['categories'] as $catagorieID):
			include_once'../App/Modeles/postsModele.php';
			$returnUpadteCategorie = posts\updateCategorieByPostId($connexion, [
				'postId' => $_GET['id'],
				'categorie' => $catagorieID
			]);	
		endforeach;*/
		//redirection vers la liste des posts
		
		header('location:'.ROOT.'posts');
	}




