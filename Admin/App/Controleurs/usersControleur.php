<?php 
	/*
	*  ../App/Controleur/usersControleur.php
	*
	*/

	namespace Controleur\Users;
	use Modele\Users;

	function dashboardAction(\PDO $connexion){
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = TITRE_USERS_DASHBOARD;
		ob_start();
			include'../App/Vues/Users/dashboard.php';
		$content = ob_get_clean();
	}

	function logoutAction(){
		//je tue la connexion de session 'user
		unset($_SESSION['users']);
		
		//je redige vers le site public
		header('location: '.ROOT_PUBLIC);
		
	}
	

