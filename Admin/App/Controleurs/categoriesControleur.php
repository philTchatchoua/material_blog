<?php
	/*
	*  ../App/Categories/categoriesControleur.php
	*
	*/


	namespace Controleur\Categories;
	use Modele\Categories as Categorie;
	use Modele\Posts as Post;


	/*
	*  Name function : indexAction
	*  Details: affiche la liste des categories
	*/

	function indexAction(\PDO $connexion,  array $params =[]){
	
		//je demande la liste de categorie au modèle
		include_once'../App/Modeles/categoriesModele.php';
		$categories = Categorie\findAll($connexion, $params);
		
		 GLOBAL $content, $titre;
		 $titre = TITRE_LISTE_CATEGORIES;
		 ob_start();
		//je charge la vue dans la variable $content
			include'../App/Vues/Categories/index.php';
		$content = ob_get_clean();
	}
	
	
	/*
	*  Name function :addAction
	*  Details: affichage du formulaire d'ajout d'enregistrement
	*/

	function addFormAction(){
		//je charge la vue dans la variable $content
		GLOBAL $content, $titre;
		$titre = TITRE_FORM_ENREGISTREMENT;
		ob_start();
			include'../App/Vues/Categories/addForm.php';
		$content = ob_get_clean();
	}

	/*
	*  Name function :insertAction
	*  Details: Ajout d'enregistrement
	*/

	function insertAction(\PDO $connexion, array $data = null){
		//je charge le modele des categories
		include_once'../App/Modeles/categoriesModele.php';
		$id = Categorie\insertOne($connexion, $data);
			
		//je fais une redirection vers la liste des categories
			header('location: '. ROOT .'categories');
		
	}

	
	/*
	*  Name function :deleteAction
	*  Details: suppressio d'enregistrement
	*/

	function deleteAction(\PDO $connexion, int $data){
		
		//je charge le modele des categories
		include_once'../App/Modeles/categoriesModele.php';
		$return = Categorie\deleteOne($connexion, $data);
		
		//je fais une redirection vers la liste des categories
		header('location: '. ROOT .'categories');
		
			
	}



	/*
	*  Name function :editAction
	*  Details: edition d'un enregistrement
	*/

	function editAction(\PDO $connexion, int $data){
	
		//je charge le modele des categories
		include_once'../App/Modeles/categoriesModele.php';
		$categorie = Categorie\editOne($connexion, $data);
		
		//je charge la vue dans la variable content
		GLOBAL $content, $titre;
		$titre = TITRE_EDIT_ENREGISTREMENT;
		ob_start();
		//je charge la vue dans la variable $content
			include'../App/Vues/Categories/editForm.php';
		$content = ob_get_clean();
				
	}
	
	/*
	*  Name function :updateAction
	*  Details: Modification d'un enregistrement
	*/

	function updateAction(\PDO $connexion, array $data = null){
		//je charge le modele des categories
		include_once'../App/Modeles/categoriesModele.php';
		$id = Categorie\updateOne($connexion, $data);
			
		//je fais une redirection vers la liste des categories
			header('location: '. ROOT .'categories');
		
	}

	




























