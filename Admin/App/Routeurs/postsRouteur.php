<?php 

	/*
	*  ../App/Routeur/postsRouteur.php
	*
	*/

	use Controleur\posts;
	include_once'../App/Controleurs/postsControleur.php';

 	switch ($_GET['posts']):
		// UPDATE D'UN POST 
		// PATTERN:/index.php?posts=update&id=XXX
		// CTRL: postsControleur
		// ACTION : update
		case'update':
			posts\updateAction($connexion);
				
		break;

		// AFFICHAGE D'EDITION D'UN POST 
		// PATTERN:/index.php?posts=edit&id=XXX
		// CTRL: postsControleur
		// ACTION : edit
		case'edit':
			posts\editAction($connexion,$_GET['id']);
		break;

		// SUPPRESSION D'UN POSTS 
		// PATTERN:/index.php?posts=delete&id=XXX
		// CTRL: postsControleur
		// ACTION : delete
		case'delete':
			posts\deleteAction($connexion,$_GET['id']);
		break;

		// AFFICHAGE DU FORMULAIRE 
		// PATTERN:/index.php?posts=addForm
		// CTRL: postsControleur
		// ACTION : addForm
		case'addForm':
			posts\addFormAction($connexion);
		break;

        // INSERTION DU FORMULAIRE 
		// PATTERN:/index.php?posts=insertForm
		// CTRL: postsControleur
		// ACTION : insertForm
		case'insertForm':
			posts\insertFormAction($connexion);
		break;

		// ROUTE PAR DEFAUT 
		// PATTERN:/index.php?posts
		// CTRL: postsControleur
		// ACTION :INDEX
 		default:
			posts\indexAction($connexion, [
				'orderBy'  =>"id",
				'orderSens'=>"desc"
			]);
		break;
 	endswitch;

