<?php 

	/*
	*  ../App/Routeur/categorieRouteur.php
	*
	*/

	use Controleur\Categories;
	include_once'../App/Controleurs/categoriesControleur.php';

	switch ($_GET['categories']):

		case 'index':
			// AFFICHAGE la liste des categories
			// PATTERN:/index.php?categories=index
			// CTRL: categoriesControleur
			// ACTION : index
			Categories\indexAction($connexion, $params=[]);	
			break;

		case 'addForm':
			// AFFICHAGE DU FORMULAIRE 
			// PATTERN:/index.php?categories=addForm
			// CTRL: categoriesControleur
			// ACTION : addForm
			Categories\addFormAction();	
			break;

		case 'insert':
			// INSERT DU FORMULAIRE
			// PATTERN:/index.php?categories=insert
			// CTRL: categoriesControleur
			// ACTION : insert
			Categories\insertAction($connexion, [
				'titre' =>$_POST['titre']
			]);	
			break;

		case 'delete':
			// SUPPRESSION DU FORMULAIRE
			// PATTERN:/index.php?categories=delete&id=XXX
			// CTRL: categoriesControleur
			// ACTION : delete
			Categories\deleteAction($connexion, $_GET['id']);	
			break;
		
		case 'edit':
			// EDITION DES CATEGORIES
			// PATTERN:/index.php?categories=edit&id=XXX
			// CTRL: categoriesControleur
			// ACTION : edit
			Categories\editAction($connexion, $_GET['id']);	
			break;
		case 'update':
			// MISE A JOUR DE LA CATEGORIE
			// PATTERN:/index.php?categories=update&id=XXX
			// CTRL: categoriesControleur
			// ACTION : update
			Categories\updateAction($connexion, [
				'titre' =>$_POST['titre'],
				'id'=>$_GET['id']
			]);	
			break;

	endswitch;